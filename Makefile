
# PD-LOCUSONUS EXTERNAL LIBRARY
# -----------------------------
# This file is based on the Makefile from pd-lib-builder written by
# Katja Vetter @ https://github.com/pure-data/pd-lib-builder
#
#
# OSX
# - compile ffmpeg ....... . ./lib/ffmpeg/build-dep-osx.sh

# Debian based
# - update system first .... sudo apt-get update
# - install requirements ... sudo apt-get install make gcc puredata
# - compile ffmpeg ......... ./lib/ffmpeg/build-dep-linux-debian.sh

# Arch Linux based
# - update system first .... sudo pacman -Syu
# - install requirements ... sudo pacman -S make gcc puredata
# - compile ffmpeg ......... ./lib/ffmpeg/build-dep-linux-arch.sh


lib.name = locusonus

class.sources = locusonus.c locusamp~.c

datadirs = examples
datafiles = \
	locusamp~-help.pd \
	locusonus-meta.pd \
	LICENSE.txt \
	README.md

tar.name = pd-$(lib.name)
tar.dir  = ./dist

git.dir  = pd-$(lib.name)





################################################################################

# -- OSX
define forDarwin

  	# pd-0.48-0 
	#PDINCLUDEDIR = $(firstword $(wildcard /Applications/Pd*.app/Contents/Resources/include))
	# pd-0.48-1 (or greater ?)
	PDINCLUDEDIR = $(firstword $(wildcard /Applications/Pd*.app/Contents/Resources/src))


	# FFmpeg
	cflags = -I./lib/ffmpeg/include
	ldflags = -L./lib/ffmpeg/lib \
			-lavcodec -lavformat -lswscale -lswresample -lavutil \
			-lmp3lame -logg -lvorbis -lvorbisenc -lvorbisfile -lopus \
			-liconv -lbz2 -lz \
			-framework AudioToolbox -framework Security -framework Cocoa \
			-framework VideoToolbox -framework VideoDecodeAcceleration -framework CoreMedia -framework QuartzCore \
			-read_only_relocs suppress -undefined dynamic_lookup

	# archive name suffix
	tar.suffix = osx

endef

# -- LINUX
define forLinux

	PDLIBDIR = $(HOME)/Documents/Pd/externals

	# debian
	ifneq ($(shell grep -s "debian" /usr/lib/os-release),)
		tar.suffix = debian
		cflags = -I./lib/ffmpeg/include -Wno-sign-compare
		ldlibs = -lpthread -Wl,-Bsymbolic \
				 ./lib/ffmpeg/lib/libavformat.a \
				 ./lib/ffmpeg/lib/libavcodec.a \
				 ./lib/ffmpeg/lib/libswresample.a \
				 ./lib/ffmpeg/lib/libswscale.a \
				 ./lib/ffmpeg/lib/libavutil.a \
				 ./lib/ffmpeg/lib/libmp3lame.a \
				 ./lib/ffmpeg/lib/libvorbis.a \
				 ./lib/ffmpeg/lib/libvorbisenc.a \
				 ./lib/ffmpeg/lib/libvorbisfile.a \
				 ./lib/ffmpeg/lib/libogg.a \
				 ./lib/ffmpeg/lib/libopus.a \
				 -lz -llzma
	endif

	# raspbian
	ifneq ($(shell grep -s "raspbian" /usr/lib/os-release),)
		tar.suffix = raspbian
		cflags = -I./lib/ffmpeg/include -Wno-sign-compare
		ldlibs = -lpthread -lz \
				 ./lib/ffmpeg/lib/libavformat.a \
				 ./lib/ffmpeg/lib/libavcodec.a \
				 ./lib/ffmpeg/lib/libswresample.a \
				 ./lib/ffmpeg/lib/libswscale.a \
				 ./lib/ffmpeg/lib/libavutil.a \
				 /usr/lib/arm-linux-gnueabihf/libmp3lame.a \
				 /usr/lib/arm-linux-gnueabihf/libvorbis.a \
				 /usr/lib/arm-linux-gnueabihf/libvorbisenc.a \
				 /usr/lib/arm-linux-gnueabihf/libvorbisfile.a \
				 /usr/lib/arm-linux-gnueabihf/libogg.a \
				 /usr/lib/arm-linux-gnueabihf/libopus.a

	endif

	# archlinuxarm
	ifneq ($(shell grep -s "archlinuxarm" /usr/lib/os-release),)
		tar.suffix = archlinuxarm
		cflags = -I./lib/ffmpeg/include
		ldlibs = -lpthread -lz \
				 ./lib/ffmpeg/lib/libavformat.a \
				 ./lib/ffmpeg/lib/libavcodec.a \
				 ./lib/ffmpeg/lib/libswresample.a \
				 ./lib/ffmpeg/lib/libswscale.a \
				 ./lib/ffmpeg/lib/libavutil.a \
				 ./lib/ffmpeg/lib/libmp3lame.a \
				 ./lib/ffmpeg/lib/libvorbis.a \
				 ./lib/ffmpeg/lib/libvorbisenc.a \
				 ./lib/ffmpeg/lib/libvorbisfile.a \
				 ./lib/ffmpeg/lib/libogg.a \
				 ./lib/ffmpeg/lib/libopus.a

	endif
endef

# -- Windows
define forWindows
endef




PDLIBBUILDER_DIR=pd-lib-builder/
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder




################################################################################
### rules: tar targets #########################################################
################################################################################

ifeq ($(tar.dir),)
	tar.dir := .
endif

ifeq ($(tar.suffix),)
	tar.suffix := $(system)-$(machine)
endif


version := $(shell sed -n 's|^\#X text [0-9][0-9]* [0-9][0-9]* .* - version \(.*\);|\1|p' locusonus-meta.pd)
tar.file = $(shell echo $(tar.name)-$(version)-$(tar.suffix)-$(target.arch).tar.gz | tr A-Z a-z)

%.tar.gz :
	$(info ++++ info: create archive $@)
	@mkdir -p $(tar.dir)
	tar -cvzf $@ --exclude='.*' $(executables) $(datafiles) $(datadirs)

# create archive
tar : all $(tar.dir)/$(tar.file)

# remove archive
tarclean:
	rm -f $(tar.dir)/$(tar.file)
