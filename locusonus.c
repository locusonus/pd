/* ------------------------- locusonus ---------------------------------------- */
/*                                                                              */
/* pd-locusonus external library for Pure Data                                  */
/* Written by Grégoire Lauvin <greglauvin@gmail.com> and Stéphane Cousot        */
/* <stef@ubaa.net>                                                              */
/* Get source at http://locusonus.org/pd/                                       */
/*                                                                              */
/* For the moment this file does not execute anything, but allows to remove the */
/* message: locusonus: can't load library                                       */
/*                                                                              */
/* ---------------------------------------------------------------------------- */
/*                                                                              */
/* FFmpeg based multi format streaming client.                                  */
/* Copyright (C) 2017 Grégoire Lauvin and Stéphane Cousot.                      */
/*                                                                              */
/* This program is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by         */
/* the Free Software Foundation, either version 3 of the License, or            */
/* (at your option) any later version.                                          */
/*                                                                              */
/* This program is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/* GNU General Public License for more details.                                 */
/*                                                                              */
/* You should have received a copy of the GNU General Public License            */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>.        */
/* ---------------------------------------------------------------------------- */

#include "m_pd.h"
#include "locusonus.h"


#define LOCUSONUS_VERSION "1.0"

static char *locusonus_message = "\n# Locusonus PD external - version %s\n# Grégoire Lauvin and Stéphane Cousot, 2017 Locus Sonus reseach lab\n# GNU General Public License version 3\n";

/* -------------------------------------- locusonus --------------------------------------------- */

static t_class *locusonus_class;

typedef struct _locusonus
{
	t_object x_obj;
}
t_locusonus;


/////// SETUP //////////////////////////////////////////////////////////////////////////////////////

static void locusonus_free()
{
}

static void *locusonus_new()
{
	// t_locusonus *x = (t_locusonus*)pd_new( locusonus_class );
	// return (void *)x;
	return NULL;
}

void locusonus_setup( void )
{
	post( locusonus_message, LOCUSONUS_VERSION );

	// create class object
	locusonus_class = class_new(gensym("locusonus"), 
		(t_newmethod)locusonus_new, (t_method)locusonus_free, sizeof(t_locusonus), 
		0, 0
	);


	// register object
	//locusamp_tilde_setup();
}

