#!/bin/sh
#
# Compile FFmpeg for the pd-locusonus external libray under OSX (universal static libraries)
# based on How To @ http://www.ffmpegmac.net/HowTo/
#



# stop on errors
set -e


OS_VERSION=$(sw_vers -productVersion)
echo "--- BUILD ON OSX VERSION: $OS_VERSION"


# build universal (OSX <= 10.13)
BUILD_UNIVERSAL=0

# combine all to one static library
MERGE_LIBRARIES=1


# i386 minimum deployement target
if [ $BUILD_UNIVERSAL == 1 ]; then
	DEPLOYEMENT_TARGET=10.5
	echo "--- BUILD UNIVERSAL LIBRARIES"
else
	DEPLOYEMENT_TARGET=10.9
fi
echo "--- BUILD FOR MINIMUM DEPLOYEMENT VERSION: $DEPLOYEMENT_TARGET"



# set the compiler 
export CC=clang
if [ $BUILD_UNIVERSAL == 1 ]; then
	export CFLAGS="-arch x86_64 -arch i386 -mmacosx-version-min=${DEPLOYEMENT_TARGET}"
	export LDFLAGS="-arch x86_64 -arch i386 -mmacosx-version-min=${DEPLOYEMENT_TARGET}"
else
	export CFLAGS="-arch x86_64 -mmacosx-version-min=${DEPLOYEMENT_TARGET}"
	export LDFLAGS="-arch x86_64 -mmacosx-version-min=${DEPLOYEMENT_TARGET}"
fi




# create a 2GB Ramdisk
if [ ! -d "/Volumes/FFmpeg" ]; then
	DISK_ID=$(hdid -nomount ram://3932160) 
	newfs_hfs -v FFmpeg ${DISK_ID}
	diskutil mount ${DISK_ID}
fi


# create target directories
TARGET="$(cd "$(dirname "$0")" && pwd)"
TARGET="/Volumes/FFmpeg"
SOURCE="${TARGET}/src"

mkdir -p ${INSTALL}
mkdir -p ${TARGET}
mkdir -p ${SOURCE}

# add the target bin/ directory to the $PATH 
export PATH=${TARGET}/bin:$PATH




# ------------------------------------------------------------------------------
# YASM
# http://yasm.tortall.net
# ------------------------------------------------------------------------------

YASM_VERSION=1.3.0
YASM_SRC_DIR="yasm-${YASM_VERSION}"
YASM_ARCHIVE="${YASM_SRC_DIR}.tar.gz"
YASM_SRC_URL="http://www.tortall.net/projects/yasm/releases/${YASM_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libyasm.a ]; then

	echo "--- compile Yasm library"

	cd ${SOURCE}
	if [ ! -f ${YASM_ARCHIVE} ]; then curl -O ${YASM_SRC_URL}; fi
	if [ ! -d ${YASM_SRC_DIR} ]; then tar xzpf ${YASM_ARCHIVE}; fi
	cd ${YASM_SRC_DIR}
	./configure --prefix=${TARGET} && make -j 4 && make install

else
	echo "--- nothing to do with Yasm library"
fi



# ------------------------------------------------------------------------------
# LAME
# http://lame.sourceforge.net
# ------------------------------------------------------------------------------

LAME_VERSION=3.100
LAME_SRC_DIR="lame-${LAME_VERSION}"
LAME_ARCHIVE="${LAME_SRC_DIR}.tar.gz"
LAME_SRC_URL="https://downloads.sourceforge.net/project/lame/lame/${LAME_VERSION}/${LAME_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libmp3lame.a ]; then

	echo "--- compile LAME library"

	cd ${SOURCE}
	if [ ! -f ${LAME_ARCHIVE} ]; then curl -L -O ${LAME_SRC_URL}; fi
	if [ ! -d ${LAME_SRC_DIR} ]; then tar xzpf ${LAME_ARCHIVE}; fi
	cd ${LAME_SRC_DIR}
	./configure --prefix=${TARGET} --disable-shared --enable-static && make -j 4 && make install

else
	echo "--- nothing to do with LAME library"
fi



# ------------------------------------------------------------------------------
# OGG
# https://www.xiph.org/
# ------------------------------------------------------------------------------

OGG_VERSION=1.3.4
OGG_SRC_DIR="libogg-${OGG_VERSION}"
OGG_ARCHIVE="${OGG_SRC_DIR}.tar.gz"
OGG_SRC_URL="http://downloads.xiph.org/releases/ogg/${OGG_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libogg.a ]; then

	echo "--- compile Ogg library"

	cd ${SOURCE}
	if [ ! -f ${OGG_ARCHIVE} ]; then curl -L -O ${OGG_SRC_URL}; fi
	if [ ! -d ${OGG_SRC_DIR} ]; then tar xzpf ${OGG_ARCHIVE}; fi
	cd ${OGG_SRC_DIR}
	./configure --prefix=${TARGET} --disable-shared --enable-static && make -j 4 && make install

else
	echo "--- nothing to do with Ogg library"
fi



# ------------------------------------------------------------------------------
# VORBIS
# https://www.xiph.org/
# patch: https://github.com/videolan/vlc/commit/03b3f47bda6f462533c2d8eab74ea44799053b2c
# ------------------------------------------------------------------------------

VORBIS_VERSION=1.3.6
VORBIS_SRC_DIR="libvorbis-${VORBIS_VERSION}"
VORBIS_ARCHIVE="${VORBIS_SRC_DIR}.tar.gz"
VORBIS_SRC_URL="http://downloads.xiph.org/releases/vorbis/${VORBIS_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libvorbis.a ]; then

	echo "--- compile Vorbis libraries"

	cd ${SOURCE}
	if [ ! -f ${VORBIS_ARCHIVE} ]; then curl -L -O ${VORBIS_SRC_URL}; fi
	if [ ! -d ${VORBIS_SRC_DIR} ]; then tar xzpf ${VORBIS_ARCHIVE}; fi
	cd ${VORBIS_SRC_DIR}
	./configure --prefix=${TARGET} --with-ogg-libraries=${TARGET}/lib --with-ogg-includes=${TARGET}/include/ --enable-static --disable-shared && make -j 4 && make install

else
	echo "--- nothing to do with Vorbis libraries"
fi



# ------------------------------------------------------------------------------
# OPUS
# http://www.opus-codec.org
# ------------------------------------------------------------------------------

OPUS_VERSION=1.3.1
OPUS_SRC_DIR="opus-${OPUS_VERSION}"
OPUS_ARCHIVE="${OPUS_SRC_DIR}.tar.gz"
OPUS_SRC_URL="http://downloads.xiph.org/releases/opus/${OPUS_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libopus.a ]; then

	echo "--- compile Opus library"

	cd ${SOURCE}
	if [ ! -f ${OPUS_ARCHIVE} ]; then curl -L -O ${OPUS_SRC_URL}; fi
	if [ ! -d ${OPUS_SRC_DIR} ]; then tar xzpf ${OPUS_ARCHIVE}; fi
	cd ${OPUS_SRC_DIR}
	./configure --prefix=${TARGET} --disable-shared --enable-static && make -j 4 && make install

else
	echo "--- nothing to do with Opus libraries"
fi



# ------------------------------------------------------------------------------
# PKG-CONFIG
# https://www.freedesktop.org/wiki/Software/pkg-config/
# ------------------------------------------------------------------------------

unset LDFLAGS CFLAGS

PKG_CONFIG_VERSION=0.29.2
PKG_CONFIG_SRC_DIR="pkg-config-${PKG_CONFIG_VERSION}"
PKG_CONFIG_ARCHIVE="${PKG_CONFIG_SRC_DIR}.tar.gz"
PKG_CONFIG_SRC_URL="https://pkg-config.freedesktop.org/releases/${PKG_CONFIG_ARCHIVE}"

if [ ! -f ${TARGET}/bin/pkg-config ]; then

	echo "--- compile pkg-config"

	cd ${SOURCE}
	if [ ! -f ${PKG_CONFIG_ARCHIVE} ]; then curl -L -O ${PKG_CONFIG_SRC_URL}; fi
	if [ ! -d ${PKG_CONFIG_SRC_DIR} ]; then tar xzpf ${PKG_CONFIG_ARCHIVE}; fi
	cd ${PKG_CONFIG_SRC_DIR}
	./configure --silent --prefix=${TARGET} --with-pc-path=${TARGET}/lib/pkgconfig --with-internal-glib && make -j 4 && make install

else
	echo "--- nothing to do with pkg-config"
fi



# ------------------------------------------------------------------------------
# FFMPEG
# http://www.ffmpeg.org
# ------------------------------------------------------------------------------

# compiler environment

unset LDFLAGS CFLAGS MYFLAGS
export MYFLAGS="-L${TARGET}/lib -I${TARGET}/include -lstdc++ -mmacosx-version-min=${DEPLOYEMENT_TARGET}" 
export LDFLAGS="$MYFLAGS -logg -lvorbis" 
export CFLAGS="$MYFLAGS"



FFMPEG_SRC_DIR="ffmpeg"
FFMPEG_OPTIONS="--enable-gpl --enable-pthreads --enable-version3 \
			   --enable-static --disable-shared \
			   --enable-avcodec --enable-avformat --enable-swscale --enable-swresample --enable-avfilter \
			   --enable-libmp3lame --enable-libopus --enable-libvorbis \
			   --enable-filters --enable-runtime-cpudetect \
			   --disable-programs --disable-doc"
			   

# 64bits version
if [ ! -f ${TARGET}/lib/libavcodec.a ]; then

	echo "--- compile FFmpeg libraries (64bits)"

	cd ${SOURCE}
	if [ ! -d ${FFMPEG_SRC_DIR} ]; then git clone git://source.ffmpeg.org/ffmpeg.git; fi
	cd ${FFMPEG_SRC_DIR}
	./configure --prefix="${TARGET}" ${FFMPEG_OPTIONS} && make -j 4 && make install

else
	echo "--- nothing to do with FFmpeg libraries (64bits)"
fi

# combine all
if [ $MERGE_LIBRARIES == 1 ]; then

	echo "--- merging libraries (64bits)"

	cd "${TARGET}/lib"
	rm -f ffmpeg.a
	libtool -static -o ffmpeg.a *.a
fi



# 64bits only -> exit
if [ $BUILD_UNIVERSAL == 0 ]; then
	exit 0
fi


# # 32bits version
TARGET_32="${TARGET}/build/32"
if [ ! -f ${TARGET_32}/lib/libavcodec.a ]; then

	echo "--- compile FFmpeg libraries (32bits)"

	mkdir -p ${TARGET_32}
	cd ${TARGET_32}
	if [ ! -d ${FFMPEG_SRC_DIR} ]; then git clone git://source.ffmpeg.org/ffmpeg.git; fi
	cd ${FFMPEG_SRC_DIR}
	./configure --prefix="${TARGET_32}" ${FFMPEG_OPTIONS} --enable-cross-compile --extra-cflags="-arch i386" --extra-ldflags='-arch i386' --arch=x86_32 --target-os=darwin && make -j 4 && make install

else
	echo "--- nothing to do with FFmpeg libraries (32bits)"
fi

# combine all
if [ $MERGE_LIBRARIES == 1 ]; then

	echo "--- merging libraries (32bits)"

	cd "${TARGET_32}/lib"
	rm -f ffmpeg.a
	libtool -static -o ffmpeg.a *.a
fi




# merge 64 and 32bits version
# and make an universal version
cd ${INSTALL}
if [ -d "${TARGET_32}/lib" ]; then
	mkdir -p lib
	for file in ${TARGET_32}/lib/*
	do
		if [ -f $file ]; then
			name=`basename $file`
			lipo -create -output ./lib/$name ${TARGET_32}/lib/$name ${TARGET}/lib/$name
		fi
	done
	cp -rv ${TARGET_32}/lib/pkgconfig ./lib/
fi
if [ -d "${TARGET_32}/bin" ]; then
	mkdir -p bin
	for file in ${TARGET_32}/bin/*
	do
		if [ -f $file ]; then
			name=`basename $file`
			lipo -create -output ./bin/$name ${TARGET_32}/bin/$name ${TARGET}/bin/$name
		fi
	done
fi
if [ -d "${TARGET_32}/include" ]; then
	cp -rv ${TARGET_32}/include ./
fi
if [ -d "${TARGET_32}/share" ]; then
	cp -rv ${TARGET_32}/share ./
fi






