#!/bin/sh
#
# Compile FFmpeg for the pd-locusonus external libray under Debian
# Update your system first (sudo apt-get update)
# Require and will be install: gcc, make, yasm, git, Ogg/Vorbis, Opus and LAME libraries
#


# stop on errors
set -e


TARGET="$(cd "$(dirname "$0")" && pwd)"
SOURCE="ffmpeg"

FFMPEG_OPTIONS="--enable-gpl --enable-pic --enable-pthreads --enable-version3 --enable-static --disable-shared \
               --enable-avcodec --enable-avformat --enable-swscale --enable-swresample --enable-avfilter \
               --enable-libmp3lame --enable-libopus --enable-libvorbis \
               --enable-filters --enable-runtime-cpudetect \
               --disable-programs --disable-doc"


# install dependencies
sudo apt-get install make gcc yasm git libmp3lame-dev libopus-dev libvorbis-dev libogg-dev

# get FFmpeg from GIT
if [ ! -d ${SOURCE} ]; then git clone git://source.ffmpeg.org/ffmpeg.git; fi

# compile
cd ${SOURCE}
./configure --prefix="${INSTALL}" ${FFMPEG_OPTIONS} && make -j 4 && make install