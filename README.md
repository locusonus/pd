# [locusonus] Pd external

Written by Stéphane Cousot and Grégoire Lauvin, 2017 Locus Sonus reseach lab,  
pd@locusonus.org

## locusamp~ : FFmpeg based multi format streaming client

    locusamp~ usage: locusamp~ <channels> <buffer size>

    <channels>     number of outlets~, default 2, up to 64
    <buffer size>  per channel of the circular buffer in Kilobytes, default 2048 KB.

    Leftmost outlets: sound outlets, according to creation argumemts, or 2 by default
    Center outlet: status message (error, warning, metadata, codec, samplerate, channels, bitrate, format).
    Rightmost outlet: connection state. 0: disconnected, 1: connected

Knowned supported file format: Ogg Vorbis, MP3, Opus, AAC (HE-AAC), WAVE, AIFF, FLAC, WebA, Au.
